#!/usr/bin/env bash

set -e

TMPDIR="."

if [ "x" == "x$1" ]; then
  echo "usage: $0 <ino-file>"
  exit 1
fi

INOFILE=$1
echo Converting $INOFILE

CPPFILE="$TMPDIR/$(basename -s .ino $INOFILE).cpp"
echo "- to $CPPFILE"

mkdir -p $TMPDIR
particle preprocess $INOFILE --saveTo $CPPFILE

cppcheck --enable=all --error-exitcode=1 $CPPFILE
